'use strict'
const log = require('npmlog')
const pkg = require('../package.json')
//判断debug模式
log.level = process.env.LOG_LEVEL ? process.env.LOG_LEVEL : 'info'
log.heading = Object.keys(pkg.bin)[0]
log.addLevel('success', 2000, { fg: 'green', bold: true })

module.exports = log
