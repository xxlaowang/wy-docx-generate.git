
/**
 *spawn 封装windows和unix系统差异,参考http://nodejs.cn/api/child_process.html#spawning-bat-and-cmd-files-on-windows
 windows系统是通过cmd执行
 */
function spawnExec(command, args, options) {
  const isWin32 = process.platform === 'win32'
  const cmd = isWin32 ? 'cmd.exe' : command
  //"/c"参数是cmd的一个参数,表示执行完cmd命令后关闭
  const _args = isWin32 ? ['/c'].concat(command, args) : args
  const spawn = require('child_process').spawn
  return spawn(cmd, _args, options || {})
}




exports.spawnExec = spawnExec