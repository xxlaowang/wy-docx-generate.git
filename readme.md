# wy-docx-generate功能简介：

使用命令行进行excel数据转换json，或者通过excel数据通过word模板最终生成word文档。

## 安装说明：

1. 电脑安装 nodejs 建议安装 v16.18.1及以上版本。

2. 全局安装：

   npm i wy-docx-generate -g

   ## 使用说明：

1. ### 把excel文件转换成JSON数据

   #### 命令1： jsonbyxlsx

   wy-docx-generate jsonbyxlsx --input 输入文件名 --output 输出文件名 --sheetName excel的sheet名称

   例如：wy-docx-generate jsonbyxlsx --input input.xlsx --output array.json -s sheet1  使用input.xlsx的sheet1输出在当前目录生成一个 array.json文件

2. ### 通过excel文件生成word文档

   #### 命令2: createbyjson

wy-docx-generate createbyjson --template 模板文件名 --json json文件名  --output 输出文档文件名

例如：wy-docx-generate createbyjson --template template.docx --json array.json  --output mydoc.docx

 使用template.docx作为模板使用array.json作为数据源 生成mydoc.docx

#### 	命令3：createbyxlsx

 wy-docx-generate createbyxlsx --template 模板文件名 --xlsx xlsx文件名 --sheetname excel的sheet名称  --output 输出文档文件名 --datakey datakey

例如：wy-docx-generate createbyxlsx  --template template.docx --xlsx input.xlsx --sheetname sheet1 --output mydoc.docx --datakey array



模板.docx语法参考网址：

https://docxtemplater.com/docs/tag-types

https://docxtemplater.com/docs/angular-parse